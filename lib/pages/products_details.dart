import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductDetails extends StatefulWidget {
  final prod_detail_name;
  final prod_detail_image;
  final prod_detail_price;

  Color _favColor = Colors.grey;

  ProductDetails({
    this.prod_detail_image,
    this.prod_detail_name,
    this.prod_detail_price
  });
  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        flexibleSpace: Image(image: AssetImage('images/appbar_background.jpg'), fit: BoxFit.cover),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text('Detalhes'),
        actions: [
          IconButton(icon: Icon(Icons.shopping_cart, color: Colors.white), onPressed: (){})
        ],
      ),
      body: new Container(
        decoration: BoxDecoration(image: DecorationImage(image: AssetImage('images/backcomic_darkblue.jpg'), fit: BoxFit.cover)),
        child: new ListView(
          children: [
            SizedBox(height: 35.0),
            new Container(
              height: 300.0,
              decoration: BoxDecoration(image: DecorationImage(image: AssetImage(widget.prod_detail_image), fit: BoxFit.fitHeight)),
            ),
            SizedBox(height: 35.0),
            Row(
              children: [
               Container(
                 width: 320.0,
                 padding: EdgeInsets.fromLTRB(30.0, 0, 0, 0),
                 child: Text(widget.prod_detail_name, textAlign: TextAlign.justify, style: TextStyle(color: Colors.white, fontSize: 24.0))
               ),
                IconButton(
                  icon: Icon(Icons.favorite),
                  color: widget._favColor ,
                  onPressed: (){
                    setState(() {
                      if(widget._favColor == Colors.grey){
                        widget._favColor = Colors.red;
                      }else{
                        widget._favColor = Colors.grey;
                      }
                    });
                  },
                )
              ],
            ),
            SizedBox(height: 35.0),
            Text("Por apenas : R\$${widget.prod_detail_price.toStringAsFixed(2)}", textAlign: TextAlign.center, style: TextStyle(color: Colors.lightGreen, fontSize: 20.0),),
            SizedBox(height: 35.0),
            Text("Sinopse", textAlign: TextAlign.center, style: TextStyle(color: Colors.grey, fontSize: 18.0),),
            Container(
              padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 20.0),
              child: Text("Toda a sinopse de quadrinhos sera adicionada mais tarde através de APIs, assim como quase todo conteúdo seria. Mas por motivos de teste, segue uma sinopse genérica:"
                  "  Na Penitenciária do Estado de Gotham, na presença de Batman, Robin e do advogado do distrito Harvey Dent, o Dr. Hugo Strange e sua assistente Dra. Harleen Quinzel fazem um experimento para drenar Coringa, Pinguim, Charada, Egghead e Senhor Frio com o Evil Extractor."
                  " Um experimento dá errado e Harvey Dent está exposto ao gás da coleção de Evil Extractors, apesar da intervenção de Batman. Os efeitos do gás fazem com que Harvey Dent se torne Duas-Caras quando ele se torna um jogador no submundo da cidade de Gotham. Agora, Batman e Robin devem trabalhar para derrotar o Duas-Caras.",
                  textAlign: TextAlign.justify, style: TextStyle(color: Colors.grey, fontSize: 14.0)),
            ),
            Container(
              height: 80.0,
              padding: EdgeInsets.fromLTRB(50.0, 15.0, 50.0, 15.0),
              child: FlatButton(
                onPressed: (){},
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0), side: BorderSide(color: Colors.red),),
                color: Colors.red,
                child: Text("Adicionar ao carrinho", textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
              ),
            )
          ],
        ),
      ),
    );
  }
}
