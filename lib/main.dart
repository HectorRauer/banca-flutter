import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter_cloth/components/horizontal_listview.dart';
import 'package:flutter_cloth/components/products.dart';


void main(){
  runApp(
    new MaterialApp(
      debugShowCheckedModeBanner: false, //remove later
      home: HomePage()
    )
  );
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    // criação do carrosel
    Widget imageCarousel = new Container(
      height: 200.0,
      child: new Carousel(
        boxFit: BoxFit.fitHeight,
        images: [
          AssetImage('images/carnificina_absoluta1.jpg'),
          AssetImage('images/carnificina_absoluta2.jpg'),
          AssetImage('images/carnificina_absoluta3.jpg')
        ],
        autoplay: true,
        animationCurve: Curves.fastOutSlowIn,
        animationDuration: Duration(milliseconds: 1000),
        dotBgColor: Colors.transparent,
        dotSize: 3.5,
        indicatorBgPadding: 4.0,
      ),
    );

    return Scaffold(
      appBar: new AppBar(
        flexibleSpace: Image(image: AssetImage('images/appbar_background.jpg'), fit: BoxFit.cover),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text('Banca Nunes'),
        actions: [
          IconButton(icon: Icon(Icons.search, color: Colors.white), onPressed: (){}),
          IconButton(icon: Icon(Icons.shopping_cart, color: Colors.white), onPressed: (){})
        ],
      ),
      drawer: new Drawer(
        child: ListView(
          children: [
            //------- header
            new UserAccountsDrawerHeader(
                accountName: Text('Preencher com requisição'),
                accountEmail: Text('Preencher com requisição'),
              currentAccountPicture: GestureDetector(
                child: new CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(Icons.person, color: Colors.white)
                ),
              ),
              decoration: new BoxDecoration(
                image: DecorationImage(image: AssetImage('images/appbar_background.jpg'), fit: BoxFit.cover),
                color: Colors.red
              ),
            ),

            // ------- body
            InkWell(
              onTap: (){},
                child: ListTile(
                  title: Text('Home Page'),
                  leading: Icon(Icons.home),
                )
            ),

            InkWell(
                onTap: (){},
                child: ListTile(
                  title: Text('Quadrinhos'),
                  leading: Icon(Icons.book),
                )
            ),

            InkWell(
                onTap: (){},
                child: ListTile(
                  title: Text('Colecionáveis'),
                  leading: Icon(Icons.toys),
                )
            ),

          ],
        )
      ),
      body: new Container(
        decoration: BoxDecoration(image: DecorationImage(image: AssetImage('images/backcomic_darkblue.jpg'), fit: BoxFit.cover)),
        child: new ListView(
          children: [
            SizedBox(height: 35.0),// padding
            Container(
              padding: EdgeInsets.fromLTRB(30.0, 0.0, 0.0, 8.0),
                child: Text('Destaques', textAlign: TextAlign.start, style: TextStyle(color: Colors.white, fontSize: 22.0))
            ),
            SizedBox(height: 35.0),//padding
            imageCarousel,
            SizedBox(height: 35.0),//padding
            Text('Categorias', textAlign: TextAlign.center ,style: TextStyle(color: Colors.white, fontSize: 18.0),),
            SizedBox(height: 20.0),//padding
            HorizontalList(),
            SizedBox(height: 35.0),//padding
            Container(
                padding: EdgeInsets.fromLTRB(30.0, 0.0, 0.0, 8.0),
                child: Text('Recentes', textAlign: TextAlign.start, style: TextStyle(color: Colors.white, fontSize: 22.0))
            ),
            //gridview
            Container(
              height: 520.0,
              child: Products(),
            )
          ],
        ),
      ),
    );
  }
}
