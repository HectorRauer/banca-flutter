
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_cloth/pages/products_details.dart';


class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  var product_list = [
    {
      "name": "Batman #89",
      "image": "images/comics/batman.jpg",
      "price": 16.90,
    },
    {
      "name": "X-Men #65",
      "image": "images/comics/xmen.jpg",
      "price": 21.90,
    },
    {
      "name": "Liga da Justiça #89",
      "image": "images/comics/justice_league.jpg",
      "price": 20.90,
    },
    {
      "name": "Deadpool #19",
      "image": "images/comics/deadpool.jpg",
      "price": 32.90,
    },
    {
      "name": "Spiderman #56",
      "image": "images/comics/spiderman.jpg",
      "price": 20.90,
    },
    {
      "name": "Lobo #39",
      "image": "images/comics/lobo.jpg",
      "price": 20.90,
    },
    {
      "name": "Superman #486",
      "image": "images/comics/superman.jpg",
      "price": 20.90,
    },
    {
      "name": "Wolverine #122",
      "image": "images/comics/wolverine.jpg",
      "price": 20.90,
    },
  ];
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: product_list.length,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index){
          return Single_prod(
            prod_name: product_list[index]['name'],
            prod_image: product_list[index]['image'],
            prod_price: product_list[index]['price']
          );
    });
  }
}
class Single_prod extends StatelessWidget {
  final prod_name;
  final prod_image;
  final prod_price;

  Single_prod({
    this.prod_name,
    this.prod_image,
    this.prod_price
});
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
        tag: prod_name,
        child: Material(
          child: InkWell(
              onTap: ()=> Navigator.of(context)
                  .push(new MaterialPageRoute(builder: (context) => new ProductDetails(
                //passando valores para proxima activity
                prod_detail_image: prod_image,
                prod_detail_name: prod_name,
                prod_detail_price: prod_price,
              ))),//go to product details activity
          child: GridTile(
              footer: Container(
                color: Colors.black54,
                child: ListTile(
                  title: Text(prod_name, style: TextStyle(color: Colors.white)),
                  subtitle: Text(" R\$ ${prod_price.toStringAsFixed(2)}", style: TextStyle(color: Colors.lightGreen, fontSize: 12), textAlign: TextAlign.start,)
              ),
              ),
              child: Image.asset(prod_image, fit: BoxFit.cover,)))
          ,),
      ),
    );
  }
}

