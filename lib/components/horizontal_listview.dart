import 'package:flutter/material.dart';

class HorizontalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          Category(
            image_location: 'images/catg/fire.png',
            image_caption: 'Popular',
          ),
          Category(
            image_location: 'images/catg/star.png',
            image_caption: 'Favoritos',
          ),
          Category(
            image_location: 'images/catg/fist.png',
            image_caption: 'Ação',
          ),
          Category(
            image_location: 'images/catg/searcher.png',
            image_caption: 'Suspense',
          ),
          Category(
            image_location: 'images/catg/comedy.png',
            image_caption: 'Comédia',
          ),
          Category(
            image_location: 'images/catg/sweat-or-tear-drop-outline.png',
            image_caption: 'Drama',
          ),
        ],
      ),
    );
  }
}
class Category extends StatelessWidget {
  final String image_location;
  final String image_caption;

  Category({
    this.image_location,
    this.image_caption
  });
  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.all(6.0),
    child: InkWell(onTap: (){},
      child: Container(
        width: 100.0,
          height: 280.0,
          child: ListTile(
            title: Image.asset(image_location, width: 60.0, height: 40.0,),
             subtitle: Container(
               height: 100.0,
               padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                  alignment: Alignment.topCenter,
                  child: Text(image_caption, style: TextStyle(color: Colors.white), locale: Locale.fromSubtags(languageCode: 'pt', countryCode: '55'),),
             )
          ),
        ),
      ),
    );
  }
}

